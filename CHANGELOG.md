# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.2.0] - 2024-01-09
## Added
- Informational headers of IDS context forwarded to backends

## Changed
- Dependency updates
- JVM update to 21

## [2.1.1] - 2023-10-03
### Fixed
- Local agent use agent ID instead of connector ID
- Updated frontend dependencies

### Added
- More columns for self descriptions. Per agent you can now configure:
  - spatialCoverage
  - temporalCoverage
  - keywords
  - contentStandard

## [2.1.0] - 2022-12-13
### Changed
- Changed naming convention of `backend` to consistent lowercase
- Library versions update
- Embedded UI update
  - Agent Management
  - Test utility
  - Policy management update
  - Policy tester
- Policy Enforcement synchronization with the Core Container

### Added
- Custom ObjectMapper that allows case-insensitive properties
- Added OpenApiConfig retrieval via management API
- MongoDB compatibility of case-insensitive properties on startup

## [2.0.0] - 2022-10-04
### Changed
- Library versions update
- Gradle build script improvements
- Embedded MongoDB version update for tests

### Removed
- Spring Sleuth tracing support since this is not available anymore for newer versions of Spring Boot