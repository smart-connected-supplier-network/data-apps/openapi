# TNO IDS OpenAPI Data App

This repository contains the data app that converts OpenAPI calls to the IDS annotated message structure and vice-versa.

The data app can either be:
* **server & client**: both receive and send messages from/to other connectors. This is the default behaviour of the data app.
* **client**: only receiving messages from other connectors that will be forwarded to the backend system but not being able to forward messages from the backend system to other connectors.

The OpenAPI data app is capable of handling contract negotiation to allow fine-grained access and usage control rules to be agreed upon. These contracts can then be enforced by a Policy Enforcement Framework, e.g. the embedded Policy Enforcement Framework of the TSG Core Container.

## Deployment

Deployment of the OpenAPI data app follows for most configurations the [Connector Helm Chart](https://gitlab.com/tno-tsg/helm-charts/connector). Which is the default deployment scenario for the TSG Core Container combined with the OpenAPI data app. 

The helm Chart contains the basic configuration necessary to setup the TNO OpenAPI data app along with the TNO TSG Core Container. Only the specific configuration for the OpenAPI data app is required to be specified.

In the Connector Helm chart an example is provided that configures the core container with the OpenAPI data app, which can be found in the [`examples`](https://gitlab.com/tno-tsg/helm-charts/connector/-/tree/master/examples/default-openapi-deployment) folder.

## Interacting with the Data App
The generic structure of interactions between two connectors with both OpenAPI data apps configured is as follows:

```mermaid
flowchart TB
    subgraph Consumer
    direction LR
    A[HTTP Client] --> B(OpenApi Data App)
    B --> C(TSG Core Container)
    end
    subgraph Provider
    direction LR
    D(TSG Core Container) --> E(OpenAPI Data App)
    E --> F[HTTP Server]
    end
    Consumer --> Provider
```

As client, the backend system calls the OpenAPI Data App of its own connector. By providing the Forward-ID and Forward-Sender headers, for respectively the Agent ID of the intended receiver and the Agent ID of the sender of the message.

The endpoint of the OpenAPI Data App for forwarding the requests depends on the deployment. In general, the `/openapi` endpoint is available on port `8080`. In case of Ingress configuration it will be something like `https://{{INGRESS_HOST}}/data-app/openapi`. The endpoint requires as first path variable the version of the OpenAPI you want to access, e.g. `1.0.2` for the SCSN API. This is the base URL of the request, so specific OpenAPI endpoints should be appended to this URL.

An example for creating an order following version `1.0.2` is:
```
POST /openapi/1.0.2/order HTTP/1.1
Host: {{INGRESS_HOST}}
Forward-ID: urn:scsn:agentA
Forward-Sender: urn:scsn:agentB
...

{{BODY}}
```


## Configuration
The configuration of the data app must be placed in the `containers` list in the `values.yaml` file. Below the example configuration is shown. Most of the keys in the container section is generic for all data apps. The specific configuration of the OpenAPI data app can be found under the `config` key. 
```yaml
containers:
  - # -- Container Type. Either `data-app` (for data-app containers healthchecks and configuration are configured out of the box) or `helper`
    type: data-app
    # -- Container Name
    name: openapi-data-app
    # -- Container Image (including tag)
    image: docker.nexus.dataspac.es/data-apps/openapi-data-app:feature-policy-enforcement
    # -- Core Container API Key
    apiKey: APIKEY-...
    validateResources:
      # -- Validate state of resource metadata present at the core container
      enabled: true
      # -- Validation interval in milliseconds
      interval: 30000
    services:
    - # -- Port number for both container as services
      port: 8080
      # -- Port name
      name: http
      ingress:
        # -- Ingress path
        path: /data-app/(.*)
        # -- Ingress rewrite target
        rewriteTarget: /$1
        # -- Ingress clusterIssuer for automatic TLS certificate request by Cert Manager
        clusterIssuer: letsencrypt
        # -- Ingress clusterIssuer for automatic TLS certificate request by Cert Manager
        annotations:
          # -- Ingress authentication
          nginx.ingress.kubernetes.io/auth-url: http://{{ template "tsg-connector.fullname" . }}.{{ .Release.Namespace }}.svc.cluster.local:8082/auth/basic
    # -- Extra IDS configuration properties
    idsConfig:
      # -- Enable the retrieval of Federated Brokers
      retrieveFederatedBrokers: true
    # -- Data App Configuration. Will be inserted as the application.yaml of the Data App and therefore needs an equal structure
    config:
      # -- OpenAPI Data App configuration
      openApi:
        usePolicyEnforcement: true
        openApiBaseUrl: https://api-specs.ids.smart-connected.nl/specs/scsn/
        versions:
          - 1.0.2
        agents:
        - backendUrl: https://httpbin.org/anything
          config:
            cac:PartyLegalEntity:
              cbc:CompanyID: "34567876543"
            cac:PartyTaxScheme:
              cbc:CompanyID: "345678765431"
            connector:
              SUPPORTEDMESSAGES: Order2.0;OrderResponse2.0;OrderStatus2.0
          id: urn:scsn:34567890
          title: TNO SCSN Test Buyer Agent A Test
```

This example shows the configuration of an OpenAPI data app configured with the Smart Connected Supplier Network API, with a single agent connected to the data app.

The full configuration options of the OpenAPI are:
| <div style="width: 400px">Key</div> | <div style="width: 50px">Type</div> | <div style="width: 300px">Default</div> | Description |
|-----|------|---------|-------------|
| isConsumerOnly | bool | `false` | (Optional) Act as consumer only and do not provide any server capabilities |
| strictApi | bool | `true` | (Optional) Strictly adhere to the OpenAPI specification and filter out any HTTP headers and query parameters that are not in the specification. |
| validate | bool | `true` | Validate requests based on the API specification and block any unknown requests |
| usePolicyEnforcement | bool | `false` | Deploy the data app with policy enforcement capabilities, primarily for negotiating policies and communicating with the Policy Enforcement Framework to administer the policies for the data app |
| mapping | map<string, string> | `nil` | (Optional) Custom mapping for HTTP headers, to find the corresponding connector and agent identifier. The keys in the map are the HTTP headers, the values are a SPARQL query snippet, with `{{QUERY}}` being replaced with the value of the HTTP header  |
| openApiBaseUrl | string | `nil` | (Optional) OpenAPI base URL, see the section below on how to configure the API and agents |
| backendBaseUrl | string | `nil` | (Optional) Backend base URL, see the section below on how to configure the API and agents |
| backendBaseUrlMapping | map<string, string> | `nil` | (Optional) Backend base URL mapping, see the section below on how to configure the API and agents |
| versions | string[] | `nil` | (Optional) API versions list, see the section below on how to configure the API and agents |
| agents[].id | string | `""` |  Agent identifier, in URI format |
| agents[].title | string | `nil` |  (Optional) Agent title |
| agents[].config | map<string, any> | `nil` |  (Optional) Additional configuration or metadata parameters |
| agents[].backendUrl | string | `nil` |  (Optional) Backend base URL, see the section below on how to configure the API and agents |
| agents[].backendUrlMapping | map<string, string> | `nil` |  (Optional) Backend base URL mapping, see the section below on how to configure the API and agents |
| agents[].versions | string[] | `nil` |  (Optional) API versions list, see the section below on how to configure the API and agents |
| agents[].openApiBaseUrl | string | `nil` |  (Optional) API versions list, see the section below on how to configure the API and agents |
| agents[].spatialCoverage | map<latitude: float, longitude:float> | `nil` | (Optional) provide spatial coverage for the data service |
| agents[].temporalCoverage | map<begin: float, end:float> | `nil` | (Optional) provide temporal coverage for the data service |
| agents[].keywords | string[] | `nil` | (Optional) keywords for the data service |
| agents[].contentStandard | string | `nil` | (Optional) URL that points to the contentStandard. |
| mongoDbConfig | object | `nil` | (Optional) MongoDB configuration for persistent storage of agents | 
| mongoDbConfig.hostname | string | `""` | MongoDB hostname |
| mongoDbConfig.hostname | int | `27017` | MongoDB port number |
| mongoDbConfig.noAuth | bool | `false` | Connect without authentication |
| mongoDbConfig.username | string | `nil` | (Optional) Username |
| mongoDbConfig.password | string | `nil` | (Optional) Password |
| mongoDbConfig.authenticationDatabase | string | `nil` | (Optional) Authentication database |
| mongoDbConfig.database | string | `""` | Database used for agent collection |
| mongoDbConfig.collection | string | `""` | Collection used for agents |
| mongoDbConfig.isSslEnabled | bool | `false` | Connect to MongoDB via SSL/TLS |
| mongoDbConfig.isWatchable | bool | `false` | Connect to MongoDB via watchable connections, only capable for replicaset MongoDB deployments |
| mongoDbConfig.pullInterval | long | `nil` | (Optional) Pull interval for the MongoDB if the MongoDB is not watchable |


## API Configuration
The configuration allows different kinds of configurations for the API versions that are used in the OpenAPI data app. A global API and versions can be used if all agents are homogeneous. But more granular version support per agent can also be configured, so that not all agents are required to support the same API specification or versions. Also, the way the versions are propagated to the backend system can be configured to further control the way the backends are called from the data app. The following examples show the different options.

### Global API and versions
In this scenario, all of the agents use the same SCSN API specification and the versions `1.0.0` and `1.1.0`. Without any deviations per agent.
```yaml
openApiBaseUrl: https://api-specs.ids.smart-connected.nl/specs/scsn/
versions:
- 1.0.0
- 1.1.0
agents:
- id: urn:scsn:agentA
  ...
  backendUrl: http://agentAbackend:8080/api
- id: urn:scsn:agentB
  ...
  backendUrl: http://agentBbackend:8080/api
```


### Different versions per Agent
In this scenario, both agents support the global version(s) (`1.0.0`) from the list and specific versions specified for respectively agent A and agent B. So agent A will support versions `0.9.0` and `1.0.0` and agent B `1.0.0` and `1.1.0`.
```yaml
openApiBaseUrl: https://api-specs.ids.smart-connected.nl/specs/scsn/
versions:
- 1.0.0
agents:
- id: urn:scsn:agentA
  ...
  versions:
  - 0.9.0
  backendUrl: http://agentAbackend:8080/api
- id: urn:scsn:agentB
  ...
  versions:
  - 1.1.0
  backendUrl: http://agentBbackend:8080/api
```

### Different API specifications per Agent
This scenario has the most deviation. For agent A, the SCSN API is used with versions `1.0.0` and `1.1.0`. For agent B, the BaSyx API is used with version `v1`.
```yaml
openApiBaseUrl: https://api-specs.ids.smart-connected.nl/specs/scsn/
versions:
- 1.0.0
- 1.1.0
agents:
- id: urn:scsn:agentA
  ...
  backendUrl: http://agentAbackend:8080/api
- id: urn:aas:agentB
  ...
  openApiBaseUrl: https://app.swaggerhub.com/apiproxy/registry/BaSyx/basyx_asset_administration_shell_repository_http_rest_api/
  versions:
  - v1
  backendUrl: http://agentBbasyx:4001/
```

## Internal backend configuration
The configuration of how the OpenAPI data app forwards messages to the configured internal backend systems that implement the API specifications is also quite flexible.

A decision can be made to support all agents via one server/router, but also fine-grained control is possible to direct requests to specific servers/endpoints. The different options are detailed below.

### Global backend URL mapping
The global backendBaseUrl can be used to forward all messages to generalized paths in the form of `{backendBaseUrl}/{agentId}/{version}` in the example below: `http://globalBackend:8080/urn:scsn:agentA/1.0.0`.

```yaml
openApiBaseUrl: https://api-specs.ids.smart-connected.nl/specs/scsn/
backendBaseUrl: http://globalBackend:8080
versions:
- 1.0.0
agents:
- id: urn:scsn:agentA
  ...
- id: urn:scsn:agentB
  ...
```

### Global backend per version
The global backendBaseUrlMapping can be used to forward all messages for specific API versions to generalized paths in the form of `{backendBaseUrlMapping}/{agentId}/` in the example below: `http://100backend:8080/urn:scsn:agentA`.

```yaml
openApiBaseUrl: https://api-specs.ids.smart-connected.nl/specs/scsn/
backendBaseUrlMapping:
  0.9.0: http://090backend:8080
  1.0.0: http://100backend:8080
versions:
- 0.9.0
- 1.0.0
agents:
- id: urn:scsn:agentA
  ...
- id: urn:scsn:agentB
  ...
```
### Agent based generic backend
The agent based backendUrl can be used to forward all messages for a specific agent to a backend, this backend will be appended with the version. For example: `http://agentAbackend:8080/0.9.0`.

```yaml
openApiBaseUrl: https://api-specs.ids.smart-connected.nl/specs/scsn/
versions:
- 0.9.0
- 1.0.0
agents:
- id: urn:scsn:agentA
  backendUrl: http://agentAbackend:8080
  ...
- id: urn:scsn:agentB
  backendUrl: http://agentBbackend:8080
  ...
```
### Agent based backend per version
This is the most configurable option, where you can specify for each agent and each version specifically what backend should be used.

```yaml
openApiBaseUrl: https://api-specs.ids.smart-connected.nl/specs/scsn/
versions:
- 0.9.0
- 1.0.0
agents:
- id: urn:scsn:agentA
  backendUrlMapping:
    0.9.0: http://090agentAbackend:8080
    1.0.0: http://100agentAbackend:8080
  ...
- id: urn:scsn:agentB
  backendUrlMapping:
    0.9.0: http://090agentBbackend:8080
    1.0.0: http://100agentBbackend:8080
  ...
```

## Implementation
The OpenAPI data app converts HTTP calls to IDS annotated messages. For this the IDS `InvokeOperationMessage` is used that contains the IDS required metadata for the data exchange between two connectors.
As body of the message, the HTTP call is converted to a JSON structure. Which is as follows:
```
POST /openapi/1.0.2/endpoint?name=TestParameter HTTP/1.1
Host: {{INGRESS_HOST}}
Forward-ID: urn:scsn:agentA
Forward-Sender: urn:scsn:agentB
Accept: application/json

TEST BODY
```
Towards:
```json
{
  "method": "GET",
  "path": "/endpoint",
  "parameters": {
     "name": "TestParameter"
  },
  "headers": {
     "accept": "application/json"
  },
  "body": "TEST BODY"
}
```

> _Note_: this structure is sub-optimal, especially for large requests or binary requests. Therefore, this might be refactored to have just the body as body of the message and the rest of the metadata as part of the `InvokeOperationMessage`.

## Build

If you do not want to use the provided Docker image (`docker.nexus.dataspac.es/data-apps/openapi-data-app:master`), you can build the OpenAPI data app yourself using Gradle:
```shell
./gradlew clean assemble
```
Some tests are implemented to verify basic functionality of the data-app:
```shell
./gradlew test
```
Via gradle Docker images can be created automatically, for both amd64 and arm64, without requirement of Docker daemon:
```shell
./gradlew jib
```
By executing this, the images will be directly pushed to the corresponding Docker registry.
If you opt for building the image just for local usage, using the Docker daemon is also possible:
```shell
./gradlew jibDockerBuild
```
