import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.springframework.boot") version LibraryVersions.spring
    id("io.spring.dependency-management") version LibraryVersions.springDeps
    id("com.google.cloud.tools.jib") version LibraryVersions.jib
    kotlin("jvm") version LibraryVersions.kotlin
    kotlin("plugin.spring") version LibraryVersions.kotlin
    kotlin("plugin.serialization") version LibraryVersions.kotlin
    id("com.github.node-gradle.node") version LibraryVersions.node
}

group = "nl.tno.ids"
version = "2.2.0"
java.sourceCompatibility = JavaVersion.VERSION_21
java.targetCompatibility = JavaVersion.VERSION_21

repositories {
    mavenLocal()
    mavenCentral()
    maven {
        url = uri("https://maven.iais.fraunhofer.de/artifactory/eis-ids-public/")
    }
    maven {
        url = uri("https://nexus.dataspac.es/repository/tsg-maven/")
    }
}

dependencies {
    implementation("nl.tno.ids:base-data-app:${LibraryVersions.tnoIds}")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa:${LibraryVersions.spring}")

    implementation("io.swagger.parser.v3:swagger-parser:${LibraryVersions.swagger}")

    implementation("org.json:json:${LibraryVersions.json}")

    implementation("org.mongodb:bson:${LibraryVersions.mongo}")
    implementation("org.mongodb:mongodb-driver-core:${LibraryVersions.mongo}")
    implementation("org.mongodb:mongodb-driver-sync:${LibraryVersions.mongo}")

    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:${LibraryVersions.kotlinXSerialization}")

    testImplementation("org.springframework.boot:spring-boot-starter-test:${LibraryVersions.spring}")
    testImplementation("de.flapdoodle.embed:de.flapdoodle.embed.mongo:${LibraryVersions.Test.embedMongo}")
    testImplementation("com.github.tomakehurst:wiremock-jre8-standalone:${LibraryVersions.Test.wiremock}")

    // Update of dependencies of dependencies
    implementation("ch.qos.logback:logback-classic:1.2.13")
    implementation("ch.qos.logback:logback-core:1.2.13")
    implementation("org.apache.commons:commons-compress:1.21")
    implementation("org.apache.jena:jena-core:4.4.0")
    implementation("org.yaml:snakeyaml:2.0")
    implementation("com.google.guava:guava:33.0.0-jre")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "21"
    }
}

/**
 * Use Google Container Tools / jib to build and publish a Docker image.
 * This task is ran using Gitlab Runners, defined in .gitlab-ci.yml
 * Publish Docker image to repository
 */
jib {
    from {
        image = "eclipse-temurin:21-jre"
        platforms {
            platform {
                architecture = "amd64"
                os = "linux"
            }
            platform {
                architecture = "arm64"
                os = "linux"
            }
        }
    }

    extraDirectories {
        paths {
            path {
                setFrom("${project.projectDir}/frontend/dist")
                into = "/static"
            }
        }
    }

    // Resulting image name & auth for registry
    to {
        val imageName = System.getenv().getOrDefault("IMAGE_NAME", "docker.nexus.dataspac.es/data-apps/openapi-data-app")
        val imageTag = System.getenv().getOrDefault("IMAGE_TAG", "develop").replace('/', '-')
        val imageTags = mutableSetOf(imageTag)
        if (System.getenv().containsKey("CI") && System.getenv().containsKey("CI_COMMIT_SHORT_SHA")) {
            imageTags += "${imageTag}-${System.getenv("CI_COMMIT_SHORT_SHA")}"
        }
        image = imageName
        tags = imageTags
    }

    container {
        jvmFlags = listOf("-Xms512m", "-Xmx512m")
        ports = listOf("8080/tcp")
        creationTime.set("USE_CURRENT_TIMESTAMP")
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}

/**
 * NodeJS configuration for building the Vue frontend to be included in the Docker image
 */
node {
    nodeProjectDir.set(file("${project.projectDir}/frontend"))

    download.set(true)
    version.set("16.17.1")
}

tasks.npmInstall {
}

val buildVue = tasks.register<com.github.gradle.node.npm.task.NpmTask>("buildVue") {
    dependsOn(tasks.npmInstall)
    args.set(listOf("run", "build"))
}

tasks.jib.configure {
    dependsOn(buildVue)
}
tasks.jibDockerBuild.configure {
    dependsOn(buildVue)
}
