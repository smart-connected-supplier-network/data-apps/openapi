package nl.tno.ids.openapi

import com.fasterxml.jackson.databind.ObjectMapper
import com.mongodb.BasicDBObject
import com.mongodb.MongoClientSettings
import com.mongodb.MongoCredential
import com.mongodb.ServerAddress
import com.mongodb.client.MongoClients
import com.mongodb.client.MongoCollection
import com.mongodb.client.model.Filters
import com.mongodb.client.model.Updates
import com.mongodb.connection.ClusterSettings
import com.mongodb.connection.SslSettings
import de.fraunhofer.iais.eis.*
import de.fraunhofer.iais.eis.util.TypedLiteral
import nl.tno.ids.base.ResourcePublisher
import nl.tno.ids.base.configuration.IdsConfig
import nl.tno.ids.common.serialization.DateUtil
import nl.tno.ids.openapi.server.OpenAPIMessageHandler
import org.bson.codecs.configuration.CodecRegistries
import org.bson.codecs.pojo.Conventions
import org.bson.codecs.pojo.PojoCodecProvider
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import java.beans.PropertyChangeEvent
import java.beans.PropertyChangeListener
import java.net.URI
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicBoolean

@Component
class AgentsController(
    private val openApiConfig: OpenApiConfig,
    private val idsConfig: IdsConfig,
    private val resourcePublisher: ResourcePublisher,
    private val openAPIMessageHandler: OpenAPIMessageHandler
) : PropertyChangeListener {
    var agents: MutableSet<AgentConfig> = ConcurrentHashMap.newKeySet()
    private lateinit var collection: MongoCollection<AgentConfig>
    private var timerTask: TimerTask? = null
    override fun propertyChange(propertyChangeEvent: PropertyChangeEvent) {
        LOG.info("Configuration change from property change event")
    }

    fun addAgent(agentConfig: AgentConfig) {
        if (openApiConfig.mongoDbConfig != null) {
            collection.insertOne(agentConfig)
            timerTask!!.run()
        } else {
            agents.add(agentConfig)
            updateAgents(agents)
        }
    }

    fun modifyAgent(id: String, agentConfig: AgentConfig) {
        if (openApiConfig.mongoDbConfig != null) {
            collection.replaceOne(BasicDBObject("id", id), agentConfig)
            timerTask!!.run()
        } else {
            agents.removeIf { agent -> agent.id == "id" }
            agents.add(agentConfig)
            updateAgents(agents)
        }
    }

    fun removeAgent(id: String) {
        if (openApiConfig.mongoDbConfig != null) {
            collection.deleteOne(BasicDBObject("id", id))
            timerTask!!.run()
        } else {
            agents.removeIf { agent -> agent.id == id }
            updateAgents(agents)
        }
    }

    companion object {
        private val LOG = LoggerFactory.getLogger(AgentsController::class.java)
    }

    init {
        openApiConfig.mongoDbConfig?.let { mongoDbConfig ->
            val mongoClient = MongoClients.create(MongoClientSettings.builder()
                .applyToClusterSettings { builder: ClusterSettings.Builder ->
                    builder.applySettings(
                        ClusterSettings.builder()
                            .hosts(listOf(ServerAddress(mongoDbConfig.hostname, mongoDbConfig.port)))
                            .build()
                    )
                }
                .apply {
                    if (!mongoDbConfig.noAuth) {
                        credential(
                            MongoCredential.createCredential(
                                mongoDbConfig.username!!,
                                mongoDbConfig.authenticationDatabase!!,
                                mongoDbConfig.password!!.toCharArray()
                            )
                        )
                    }
                }
                .applyToSslSettings { builder: SslSettings.Builder ->
                    builder.applySettings(
                        SslSettings.builder().enabled(
                            mongoDbConfig.isSslEnabled
                        ).build()
                    )
                }
                .codecRegistry(
                    CodecRegistries.fromRegistries(
                        MongoClientSettings.getDefaultCodecRegistry(),
                        CodecRegistries.fromProviders(
                            PojoCodecProvider.builder()
                                .conventions(listOf(Conventions.ANNOTATION_CONVENTION, Conventions.USE_GETTERS_FOR_SETTERS))
                                .automatic(true)
                                .build()
                        )
                    )
                )
                .build())
            collection = mongoClient
                .getDatabase(mongoDbConfig.database)
                .getCollection(mongoDbConfig.collection, AgentConfig::class.java)

            if (collection.countDocuments() > 0) {
                // Replace any existing documents with `backEndUrlMapping` to `backendUrlMapping`
                collection.updateMany(Filters.exists("backEndUrlMapping"), Updates.rename("backEndUrlMapping", "backendUrlMapping"))
            }
            if (collection.countDocuments() == 0L && openApiConfig.agents.isNotEmpty()) {
                collection.insertMany(openApiConfig.agents)
            }

            // Use existing data in the Mongo collection
            updateAgents(collection.find().sort(BasicDBObject("id", 1)).toSet())
            val watchThread = Thread(if (mongoDbConfig.isWatchable) {
                MongoWatchThread()
            } else {
                MongoPullThread()
            })
            watchThread.start()
        } ?: updateAgents(openApiConfig.agents.toSet())
    }

    private fun updateAgents(newAgents: Set<AgentConfig>) {
        val removedAgents = agents.filter { oldAgent -> newAgents.none { oldAgent.id == it.id }}
        agents = newAgents.toMutableSet()
        openAPIMessageHandler.safeConfigure(agents)
        if (removedAgents.isNotEmpty()) {
            LOG.info("Removing ${removedAgents.size} agents")
        }
        removedAgents.forEach {
            resourcePublisher.unpublishResourceToCoreContainer("${idsConfig.connectorId}:${it.id}")
        }

        if (newAgents.isEmpty()) return

        resourcePublisher.publishResourcesToCoreContainer(agents.map { agent ->
            val versions = agent.versions.ifEmpty {
                openApiConfig.versions
            }
            ResourceBuilder()
                ._sovereign_(URI(agent.id))
                .apply {
                    agent.title?.let { _title_(TypedLiteral(it, "en")) }
                    agent.config?.let {
                        jsonToDot(it).forEach { (key, value) ->
                            // TODO: Use custom properties instead of description
                            _description_(TypedLiteral("$key=$value", "property"))
                        }
                    }
                    agent.spatialCoverage?.let {
                        _spatialCoverage_(
                            GeoPointBuilder()
                                ._latitude_(it.latitude)
                                ._longitude_(it.longitude)
                                .build())
                    }
                    agent.temporalCoverage?.let {
                        _temporalCoverage_(
                            IntervalBuilder()
                                ._begin_(InstantBuilder()
                                    ._dateTime_(DateUtil.asXMLGregorianCalendar(SimpleDateFormat("yyyy-MM-dd").parse(it.begin)))
                                    .build())
                                ._end_(InstantBuilder()
                                    ._dateTime_(DateUtil.asXMLGregorianCalendar(SimpleDateFormat("yyyy-MM-dd").parse(it.end)))
                                    .build())
                                .build()
                        )
                    }
                    agent.contentStandard?.let {
                        _contentStandard_(URI(agent.contentStandard))
                    }
                }
                ._keyword_(agent.keywords.map {
                    TypedLiteral(it, "en")
                })

                ._resourceEndpoint_(versions.map {
                    ConnectorEndpointBuilder()
                        ._endpointDocumentation_(URI((agent.openApiBaseUrl ?: openApiConfig.openApiBaseUrl) + it))
                        ._accessURL_(URI.create("http://localhost"))
                        ._path_("/${agent.id}/$it")
                        .build()
                })
                .build()
        })
    }

    private fun jsonToDot(config: Map<String, Any>, prefix: String = ""): Map<String, String> {
        val addition = mutableMapOf<String, String>()
        return config.map { (key, value) ->
            when (value) {
                is String -> "$prefix$key" to value
                is Map<*, *> -> {
                    @Suppress("UNCHECKED_CAST")
                    addition.putAll(jsonToDot(value as Map<String, Any>, "$prefix$key."))
                    null
                }
                else -> "$prefix$key" to value.toString()
            }
        }.filterNotNull().toMap().toMutableMap().apply { putAll(addition) }
    }

    inner class MongoWatchThread: Runnable {
        override fun run() {
            LOG.info("Starting watch thread")
            val updateScheduled = AtomicBoolean(false)
            val iterator = collection.watch().iterator()
            LOG.info("Started iterator")
            while (iterator.hasNext()) {
                iterator.next()
                if (!updateScheduled.get()) {
                    LOG.info("ChangeStreamDocument received, scheduling update")
                    Timer().schedule(object : TimerTask() {
                        override fun run() {
                            updateAgents(collection.find().sort(BasicDBObject("id", 1)).toSet())
                            updateScheduled.set(false)
                        }
                    }, 2000)
                    updateScheduled.set(true)
                } else {
                    LOG.info("ChangeStreamDocument received, update already scheduled")
                }
            }
        }
    }

    inner class MongoPullThread: Runnable {
        override fun run() {
            LOG.info("Starting periodic loop thread")
            timerTask = object : TimerTask() {
                override fun run() {
                    val newAgents: Set<AgentConfig> = collection.find().sort(BasicDBObject("id", 1)).toSet()
                    if (newAgents != agents) {
                        LOG.info("Difference in agents & newAgents")
                        updateAgents(newAgents)
                    }
                }
            }
            Timer().schedule(timerTask, openApiConfig.mongoDbConfig?.pullInterval ?: 60000L, openApiConfig.mongoDbConfig?.pullInterval ?: 60000L)
        }
    }
}