package nl.tno.ids.openapi

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import java.net.URI
import java.util.*
import kotlin.collections.ArrayList

fun constructAccessUrl(accessUrl: String, path: String): String {
    return "${accessUrl.dropLastWhile { it == '/' }}/${path.dropWhile { it == '/' }}"
}

fun constructAccessUrl(accessUrl: URI, path: String): String {
    return constructAccessUrl(accessUrl.toString(), path)
}

enum class AuthenticationType {
    BASIC, BEARER, CUSTOM
}

data class BackendAuthentication(
    val type: AuthenticationType = AuthenticationType.BASIC,
    val username: String? = null,
    val password: String? = null,
    val token: String? = null,
    val header: Map<String, String>? = null
)

data class SpatialCoverage(
    val latitude: Float,
    val longitude: Float
)

data class TemporalCoverage(
    val begin: String,
    val end: String
)

class AgentConfig(
    var id: String,
    var title: String? = null,
    config: Map<String, Any>? = null,
    var backendUrl: String? = null,
    var backendUrlMapping: Map<String, String> = emptyMap(),
    var versions: List<String> = ArrayList(),
    var openApiBaseUrl: String? = null,
    val backendAuthentication: BackendAuthentication? = null,
    val spatialCoverage: SpatialCoverage? = null,
    val temporalCoverage: TemporalCoverage? = null,
    val keywords: List<String> = ArrayList(),
    val contentStandard: String?= null
) {
    var config: Map<String, Any>? = config
    set(value) {
        field = value?.let { ublFormat(it) }
    }
    constructor() : this("")

    @Suppress("UNCHECKED_CAST")
    /**
     * Special characters are stripped from Spring Configuration Properties.
     * In SCSN these are used for UBL prefixes (cac & cbc)
     */
    private fun ublFormat(map: Map<String, Any>): Map<String, Any> {
        return map.map {
            it.key.replace("^(cac|cbc)([^:])".toRegex(), "$1:$2") to if (it.value is Map<*, *>) ublFormat(it.value as Map<String, Any>) else it.value
        }.toMap()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as AgentConfig

        if (id != other.id) return false
        if (title != other.title) return false
        if (backendUrl != other.backendUrl) return false
        if (backendUrlMapping != other.backendUrlMapping) return false
        if (versions != other.versions) return false
        if (config != other.config) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + (title?.hashCode() ?: 0)
        result = 31 * result + (backendUrl?.hashCode() ?: 0)
        result = 31 * result + backendUrlMapping.hashCode()
        result = 31 * result + versions.hashCode()
        result = 31 * result + (config?.hashCode() ?: 0)
        return result
    }
}

data class MongoDbConfig(
    val hostname: String,
    val port: Int = 27017,
    val noAuth: Boolean = false,
    val username: String? = null,
    val password: String? = null,
    val authenticationDatabase: String? = null,
    val database: String,
    val collection: String,
    val isSslEnabled: Boolean = false,
    val isWatchable: Boolean = false,
    val pullInterval: Long? = null
)

@ConstructorBinding
@ConfigurationProperties(prefix = "open-api")
data class OpenApiConfig(
    val isConsumerOnly: Boolean = false,
    val strictApi: Boolean = true,
    val openApiBaseUrl: String? = null,
    val backendBaseUrl: String? = null,
    val backendBaseUrlMapping: Map<String, String> = emptyMap(),
    val backendAuthentication: BackendAuthentication? = null,
    val mapping: Map<String, String> = emptyMap(),
    val versions: MutableList<String> = ArrayList(),
    val agents: List<AgentConfig> = ArrayList(),
    val mongoDbConfig: MongoDbConfig? = null,
    val validate: Boolean = true,
    val usePolicyEnforcement: Boolean = false,
    val routeLocalAgentsViaIDS: Boolean = false,
    val maskBackendUrl: Boolean = true,
    val maskReplacement: String? = null,
    val encodeInHttp: Boolean = false
)