package nl.tno.ids.openapi

import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.PathItem
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

const val BACKEND_URL_MASK = "{{IDS_BACKEND_SERVER_MASK}}"

object IDSHttpHeaders {
    const val TYPE = "X-IDS-Type"
    const val API_METHOD = "X-IDS-Method"
    const val API_PATH = "X-IDS-ApiPath"
    const val STATUS_CODE = "X-IDS-StatusCode"
    const val BACKEND_MASKED = "X-IDS-BackendMasked"
    const val HEADER_PREFIX = "X-IDS-Header-"
}

@Serializable
data class RequestObject(
    val headers: Map<String, String>,
    val method: PathItem.HttpMethod,
    val path: String,
    val parameters: Map<String, String>,
    val body: String?
) {
    val apiVersion: String by lazy {
        path.trim().split("/".toRegex(), 3).toTypedArray()[1].trim()
    }
    val apiPath: String by lazy {
        try {
            path.trim().split("/".toRegex(), 3).toTypedArray()[2].trim()
        } catch (e: ArrayIndexOutOfBoundsException) {
            ""
        }
    }

    fun encode() = Json.encodeToString(this)

    companion object {
        fun decode(json: String): RequestObject = Json.decodeFromString(json)
    }
}

@Serializable
data class SuccessResponseObject(
    val status: Int,
    val body: String,
    val headers: Map<String, String>,
    val backendMasked: Boolean
) {
    fun encode() = Json.encodeToString(this)

    companion object {
        fun decode(json: String): SuccessResponseObject = Json.decodeFromString(json)
    }
}

@Serializable
data class ErrorResponseObject(
    val reason: String,
    val message: String?,
    val status: Int = 500
) {
    fun encode() = Json.encodeToString(this)

    companion object {
        fun decode(json: String): ErrorResponseObject = Json.decodeFromString(json)
    }
}

data class LoadedOpenAPI(
    val baseUrl: String,
    val version: String,
    val openAPI: OpenAPI
)

data class AgentMapping(
    val agentConfig: AgentConfig,
    val headers: Map<String, String>,
    val versions: Map<String, Endpoint>
)

data class Endpoint(
    val openAPI: OpenAPI?,
    val backendUrl: String
)

