package nl.tno.ids.openapi

import de.fraunhofer.iais.eis.*
import nl.tno.ids.base.configuration.IdsConfig
import nl.tno.ids.common.serialization.DateUtil
import nl.tno.ids.common.serialization.SerializationHelper
import nl.tno.ids.openapi.server.OpenAPIMessageHandler
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.lang.Nullable
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException
import java.net.URI
import java.util.*

enum class Permission {
  ALLOW, DENY
}

data class Rule (
  val endpoint: String? = null,
  val version: String? = null,
  val methods: List<HttpMethod> = emptyList(),
  val permission: Permission = Permission.ALLOW,
  val assignerAgent: String? = null,
  val assigneeAgent: String? = null
)

data class PolicyOffer(
  val consumerConnector: String? = null,
  val providerConnector: String? = null,
  val startDate: Date? = null,
  val endDate: Date? = null,
  val rules: List<Rule> = emptyList()
)

data class ApiEndpoint(
  val method: String,
  val path: String
)

data class PolicyAgent(
  val id: String,
  val title: String?,
  val versions: Map<String, List<ApiEndpoint>>,
  val openApiBaseUrl: String
)

@RestController
@RequestMapping(value = ["/policies"])
class PolicyController(
  private val idsConfig: IdsConfig,
  private val openApiConfig: OpenApiConfig,
  @Nullable private val policyManager: PolicyManager?,
  private val serializationHelper: SerializationHelper,
  private val agentsController: AgentsController,
  private val openAPIMessageHandler: OpenAPIMessageHandler
) {
  @GetMapping(value = ["offers"], produces = ["application/ld+json"])
  fun getOffers(): String {
    return serializationHelper.toJsonLD(policyManager?.getOffers() ?: emptyList<ContractOffer>())
  }

  @GetMapping(value = ["contracts"], produces = ["application/ld+json"])
  fun getAgreements(): String {
    return serializationHelper.toJsonLD(policyManager?.getContracts() ?: emptyList<ContractAgreement>())
  }

  @DeleteMapping(value = ["offers"])
  fun deleteOffer(@RequestParam offerId: String): ResponseEntity<String> {
    return policyManager?.deleteContractOffer(offerId)?.let { ResponseEntity.ok("") } ?: ResponseEntity.status(HttpStatus.NOT_FOUND).body("")
  }

  @DeleteMapping(value = ["contracts"])
  fun deleteContract(@RequestParam contractId: String): ResponseEntity<String> {
    return policyManager?.deleteContractAgreement(contractId)?.let { ResponseEntity.ok("") } ?: ResponseEntity.status(HttpStatus.NOT_FOUND).body("")
  }

  @PostMapping(value = ["offers"], consumes = [MediaType.APPLICATION_JSON_VALUE, "application/ld+json"])
  fun addPolicyOffer(@RequestBody policyOffer: PolicyOffer): ContractOffer {
    if (policyManager == null) {
      throw ResponseStatusException(HttpStatus.NOT_FOUND)
    }
    val contractOfferBuilder = ContractOfferBuilder()
      ._contractStart_(policyOffer.startDate?.let { DateUtil.asXMLGregorianCalendar(it) } ?: DateUtil.now())
    policyOffer.endDate?.let {
      contractOfferBuilder._contractEnd_(DateUtil.asXMLGregorianCalendar(it))
    }
    policyOffer.consumerConnector?.let {
      contractOfferBuilder._consumer_(URI(it))
    }
    policyOffer.providerConnector?.let {
      contractOfferBuilder._provider_(URI(it))
    }
    policyOffer.rules.forEach { rule ->
      val target = "urn:ids:regex:/${rule.version ?: ".*"}/${rule.endpoint ?: ".*"}"
      when (rule.permission) {
        Permission.ALLOW -> {
          contractOfferBuilder._permission_(PermissionBuilder()
            ._action_(rule.methods.map { PolicyManager.mapMethodToAction(it) }.distinct())
            ._target_(URI(target))
            .apply {
              rule.assigneeAgent?.let {
                _assignee_(URI(it))
              }
              rule.assignerAgent?.let {
                _assigner_(URI(it))
              }
            }
            .build())
        }
        Permission.DENY -> {
          contractOfferBuilder._prohibition_(ProhibitionBuilder()
            ._action_(rule.methods.map { PolicyManager.mapMethodToAction(it) }.distinct())
            ._target_(URI(target))
            .apply {
              rule.assigneeAgent?.let {
                _assignee_(URI(it))
              }
              rule.assignerAgent?.let {
                _assigner_(URI(it))
              }
            }
            .build())
        }
      }
    }

    val contractOffer = contractOfferBuilder.build()
    policyManager.addContractOffer(contractOffer)
    return contractOffer
  }

  @GetMapping(value = ["agents"], produces = [MediaType.APPLICATION_JSON_VALUE])
  fun getAgents(): List<PolicyAgent> {
    return agentsController.agents.map { agentConfig ->
      PolicyAgent(
        id = agentConfig.id,
        title = agentConfig.title,
        versions = agentConfig.versions.ifEmpty { openApiConfig.versions }.associateWith { version ->
          openAPIMessageHandler.agentMapping[agentConfig.id]?.versions?.get(version)?.openAPI?.paths?.flatMap { (path, pathItem) ->
            pathItem.readOperationsMap().map {
              ApiEndpoint(it.key.name, path)
            }
          } ?: emptyList()
        },
        openApiBaseUrl = agentConfig.openApiBaseUrl ?: openApiConfig.openApiBaseUrl ?: ""
      )
    }
  }
}