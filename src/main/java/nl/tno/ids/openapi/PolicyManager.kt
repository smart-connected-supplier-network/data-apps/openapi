package nl.tno.ids.openapi

import de.fraunhofer.iais.eis.*
import nl.tno.ids.base.CoreContainerAPI
import nl.tno.ids.base.configuration.IdsConfig
import nl.tno.ids.base.infomodel.toJsonLD
import nl.tno.ids.common.serialization.DateUtil
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpMethod
import org.springframework.stereotype.Component
import org.springframework.web.util.UriUtils
import java.net.URI
import java.nio.charset.Charset

@Component
@ConditionalOnProperty(name = ["open-api.use-policy-enforcement"], havingValue = "true", matchIfMissing = false)
class PolicyManager(
  private val coreContainerAPI: CoreContainerAPI,
  private val idsConfig: IdsConfig
) {
  private var lastOfferRefresh: Long = 0
  private var lastContractRefresh: Long = 0
  private var offerCache: List<ContractOffer> = emptyList()
  private var contractCache: List<ContractAgreement> = emptyList()

  fun getOffers(forceRefresh: Boolean = false): List<ContractOffer> {
    if (forceRefresh || System.currentTimeMillis() - lastOfferRefresh > 1000*60) {
      offerCache = coreContainerAPI.get("pap/offers", null, object : ParameterizedTypeReference<List<ContractOffer>>() {}) ?: emptyList()
      lastOfferRefresh = System.currentTimeMillis()
    }
    return offerCache
  }

  fun getContracts(forceRefresh: Boolean = false): List<ContractAgreement> {
    if (forceRefresh || System.currentTimeMillis() - lastContractRefresh > 1000*60) {
      contractCache = coreContainerAPI.get("pap/contracts", null, object : ParameterizedTypeReference<List<ContractAgreement>>() {}) ?: emptyList()
      lastContractRefresh = System.currentTimeMillis()
    }
    return contractCache
  }

  fun addContractOffer(contractOffer: ContractOffer) {
    LOG.info("Adding contractOffer ${contractOffer.id}")
    LOG.debug(contractOffer.toJsonLD())
    coreContainerAPI.post("pap/offers", contractOffer, String::class.java)
    offerCache = offerCache + contractOffer
  }

  fun addContractAgreement(contractAgreement: ContractAgreement) {
    LOG.info("Adding contractAgreement ${contractAgreement.id}")
    coreContainerAPI.post("pap/contracts", contractAgreement, String::class.java)
    contractCache = contractCache + contractAgreement
  }

  fun deleteContractOffer(offerId: String) {
    LOG.info("Deleting contractOffer $offerId")
    coreContainerAPI.delete("pap/offers/${UriUtils.encode(offerId, Charset.defaultCharset())}")
    offerCache = offerCache.filter { it.id.toString() != offerId }
  }

  fun deleteContractAgreement(agreementId: String) {
    LOG.info("Deleting contract $agreementId")
    coreContainerAPI.delete("pap/contracts/${UriUtils.encode(agreementId, Charset.defaultCharset())}")
    contractCache = contractCache.filter { it.id.toString() != agreementId }
  }

  fun contractOfferMatches(
    apiPath: String,
    assigner: String,
    assignee: String,
    httpMethod: HttpMethod
  ) = getOffers().filter { contractOffer ->
    (contractOffer.consumer == null || contractOffer.consumer.toString() == assignee)
        && (contractOffer.provider == null || contractOffer.provider.toString() == assigner)
        && contractOffer.contractStart.compare(DateUtil.now()) < 0
        && (contractOffer.contractEnd?.compare(DateUtil.now()) ?: 1) > 0
        && (contractOffer.permission + contractOffer.prohibition).map { rule ->
      rule.target?.let { artifact ->
        if ((!artifact.toString().startsWith("urn:ids:regex:") || !artifact.toString().drop(14).toRegex().matches(apiPath)) && artifact.toString() != apiPath) {
          return@map false
        }
      }
      rule.assigner?.let { assigners ->
        if (assigners.isNotEmpty() && !assigners.contains(URI(assigner))) {
          return@map false
        }
      }
      rule.assignee?.let { assignees ->
        if (assignees.isNotEmpty() && !assignees.contains(URI(assignee))) {
          return@map false
        }
      }
      rule.action?.let { actions ->
        if (actions.isNotEmpty() && !actions.contains(mapMethodToAction(httpMethod))) {
          return@map false
        }
      }
      true
    }.let { rules -> rules.isEmpty() || rules.any { it } }
  }


  fun contractAgreementMatch(
    apiPath: String,
    assigner: String,
    assignee: String,
    httpMethod: HttpMethod
  ) = getContracts().filter { contractAgreement ->
    contractAgreement.consumer.toString() == assignee
      && (contractAgreement.provider == null || contractAgreement.provider.toString() == assigner)
      && contractAgreement.contractStart.compare(DateUtil.now()) < 0
      && (contractAgreement.contractEnd?.compare(DateUtil.now()) ?: 1) > 0
      && (contractAgreement.permission + contractAgreement.prohibition).map { rule ->
      rule.target?.let { artifact ->
        if ((!artifact.toString().startsWith("urn:ids:regex:") || !artifact.toString().drop(14).toRegex().matches(apiPath)) && artifact.toString() != apiPath) {
          return@map false
        }
      }
      rule.assigner?.let { assigners ->
        if (assigners.isNotEmpty() && !assigners.contains(URI(assigner))) {
          return@map false
        }
      }
      rule.assignee?.let { assignees ->
        if (assignees.isNotEmpty() && !assignees.contains(URI(assignee))) {
          return@map false
        }
      }
      rule.action?.let { actions ->
        if (actions.isNotEmpty() && !actions.contains(mapMethodToAction(httpMethod))) {
          return@map false
        }
      }
      true
    }.let { rules -> rules.isEmpty() || rules.any { it } }
  }


  companion object {
    private val LOG = LoggerFactory.getLogger(PolicyManager::class.java)
    fun mapMethodToAction(httpMethod: HttpMethod): Action {
      return when (httpMethod) {
        HttpMethod.GET -> Action.READ
        HttpMethod.HEAD -> Action.READ
        HttpMethod.POST -> Action.WRITE
        HttpMethod.PUT -> Action.MODIFY
        HttpMethod.PATCH -> Action.MODIFY
        HttpMethod.DELETE -> Action.DELETE
        HttpMethod.OPTIONS -> Action.READ
        HttpMethod.TRACE -> Action.READ
        else -> Action.READ
      }
    }
  }
}