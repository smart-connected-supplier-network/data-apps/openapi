package nl.tno.ids.openapi.client

import de.fraunhofer.iais.eis.InvokeOperationMessageBuilder
import de.fraunhofer.iais.eis.Message
import de.fraunhofer.iais.eis.RejectionMessage
import de.fraunhofer.iais.eis.RejectionReason
import de.fraunhofer.iais.eis.util.Util
import io.swagger.v3.oas.models.PathItem
import javax.servlet.http.HttpServletRequest
import nl.tno.ids.base.BrokerClient
import nl.tno.ids.base.HttpHelper
import nl.tno.ids.base.ResponseManager
import nl.tno.ids.base.configuration.IdsConfig
import nl.tno.ids.base.infomodel.defaults
import nl.tno.ids.base.multipart.MultiPartMessage
import nl.tno.ids.common.serialization.SerializationHelper
import nl.tno.ids.openapi.*
import nl.tno.ids.openapi.IDSHttpHeaders.API_METHOD
import nl.tno.ids.openapi.IDSHttpHeaders.API_PATH
import nl.tno.ids.openapi.IDSHttpHeaders.BACKEND_MASKED
import nl.tno.ids.openapi.IDSHttpHeaders.HEADER_PREFIX
import nl.tno.ids.openapi.IDSHttpHeaders.STATUS_CODE
import nl.tno.ids.openapi.IDSHttpHeaders.TYPE
import nl.tno.ids.openapi.pef.PolicyNegotiation
import org.slf4j.LoggerFactory
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.core.io.Resource
import org.springframework.http.*
import org.springframework.http.client.ClientHttpResponse
import org.springframework.lang.Nullable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.client.ResponseErrorHandler
import org.springframework.web.client.RestTemplate
import java.io.IOException
import java.io.InputStream
import java.net.URI
import java.util.concurrent.CompletableFuture
import java.util.regex.Pattern

class ContractNegotiationException(message: String, val rejectionMessage: RejectionMessage? = null, val payload: String? = null): Exception(message)

@RestController
@RequestMapping(value = ["/openapi"])
class OpenAPIServer(
    private val openApiConfig: OpenApiConfig,
    private val idsConfig: IdsConfig,
    private val brokerClient: BrokerClient,
    private val httpHelper: HttpHelper,
    private val responseManager: ResponseManager,
    private val agentsController: AgentsController,
    @Nullable private val policyManager: PolicyManager?,
    private val policyNegotiation: PolicyNegotiation
) {
    private val brokerPattern = Pattern.compile("(/.*)?/broker/.*")
    private val brokerSPARQLPattern: Pattern = Pattern.compile("(/.*)?/broker-sparql.*")
    private val customMappers: MutableMap<String, String> = HashMap()
    private val pathBasedRoutingRegex = "/(?<sender>(urn|http).*?)/(?<receiver>(urn|http).*?)(?<apiPath>/.*)".toRegex().toPattern()

    init {
        LOG.info("OpenAPIServer postconstruct")
        customMappers["forward-id"] =
            "?c a ids:TrustedConnector. ?c ids:resourceCatalog ?cat. ?cat ids:offeredResource ?r. ?r ids:sovereign <{{QUERY}}>"
        if (openApiConfig.mapping.isNotEmpty()) {
            customMappers.putAll(openApiConfig.mapping)
        }
        LOG.info("OpenAPIServer postconstruct end")
    }

    @RequestMapping(value = ["**"])
    fun handler(
        req: HttpServletRequest
    ): CompletableFuture<ResponseEntity<*>> {
        return CompletableFuture.supplyAsync {
            val headers: MutableMap<String, String> = mutableMapOf()
            val path = req.requestURI.replaceFirst(prefix.toRegex(), "").trim()
            val matcher = pathBasedRoutingRegex.matcher(path)
            val callingBase = openApiConfig.maskReplacement ?: "http://${req.getHeader("Host")}/openapi"
            val (apiPathRaw, callingBaseUrl) = if (matcher.find()) {
                headers["forward-sender"] = matcher.group("sender")
                headers["forward-id"] = matcher.group("receiver")
                matcher.group("apiPath") to "$callingBase/${headers["forward-sender"]}/${headers["forward-id"]}"
            } else {
                path to callingBase
            }
            val apiPath = if (apiPathRaw.count { it == '/' } < 2) {
                "$apiPathRaw/"
            } else {
                apiPathRaw
            }

            req.headerNames.asIterator().forEach { headers[it] = req.getHeader(it) }
            if (brokerSPARQLPattern.matcher(apiPath).matches()) {
                val connectors = brokerClient.queryBroker(req.inputStream.readAllBytes().decodeToString())
                    .joinToString(",") { c ->
                    try {
                        SerializationHelper.getInstance().toJsonLD(c)
                    } catch (e: IOException) {
                        ""
                    }
                }
                return@supplyAsync ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body("[$connectors]")
            }
            if ((!headers.containsKey("forward-to") || !headers.containsKey("forward-recipient"))
                    && !applyCustomMappers(headers)
                    && (!headers.containsKey("forward-to") || !headers.containsKey("forward-recipient"))) {
                return@supplyAsync errorResponse(
                    400,
                    "BAD_REQUEST",
                    "Could not map " + customMappers.keys.filter { headers.containsKey(it) }
                        .joinToString(", ") + " to Forward-To, Forward-Recipient and Forward-AccessURL"
                )
            }
            val receiver = headers["forward-to"] ?: return@supplyAsync errorResponse(
                400,
                "BAD_REQUEST",
                "No Forward-To header in request!"
            )
            val sender = headers["forward-sender"] ?: return@supplyAsync errorResponse(
                400,
                "BAD_REQUEST",
                "No Forward-Sender header in request!"
            )
            val recipient = headers["forward-recipient"] ?: return@supplyAsync errorResponse(
                400,
                "BAD_REQUEST",
                "No Forward-Recipient header in request!"
            )

            LOG.info("Received API {} call on path {}", req.method, apiPath)

            val agent = agentsController.agents.find { agentConfig -> agentConfig.id == recipient }
            val apiVersion = apiPath.dropWhile { it == '/' }.split('/').first()

            if (!openApiConfig.routeLocalAgentsViaIDS && agent !== null) {
                val backendUrl =
                    agent.backendUrlMapping.values.firstOrNull() ?: return@supplyAsync ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build<String>()
                val uri = URI.create("$backendUrl/${apiPath}".replace(Regex("(?<!(http:|https:))/+"), "/"))
                val requestHeaders = HttpHeaders()
                req.headerNames.asIterator().forEachRemaining {
                    requestHeaders[it] = req.getHeader(it)
                }
                val forward = RequestEntity(
                    req.inputStream, requestHeaders,
                    HttpMethod.valueOf(req.method), uri
                )
                val response = restTemplate.exchange(forward, Resource::class.java)
                LOG.info("Called local OpenAPI backend: $uri -> ${response.statusCode.value()}")

                val responseHeaders = HttpHeaders.writableHttpHeaders(response.headers)
                headers.remove("Transfer-Encoding")
                headers.remove("Keep-Alive")
                headers.remove("Connection")
                headers.remove("Accept-Ranges")
                 return@supplyAsync ResponseEntity
                    .status(response.statusCode)
                    .headers(responseHeaders)
                    .body(response.body)
            }

            val invokeOperationMessage = InvokeOperationMessageBuilder()
                ._operationReference_(URI.create("https://ids.tno.nl/openapi$apiPath"))
                ._issuerConnector_(URI.create(idsConfig.connectorId))
                ._senderAgent_(URI.create(sender))
                ._recipientConnector_(Util.asList(URI.create(receiver)))
                ._recipientAgent_(Util.asList(URI.create(recipient)))
                .build()
                .defaults()
            val payload = RequestObject(
                headers = headers,
                method = PathItem.HttpMethod.valueOf(req.method),
                path = apiPath,
                parameters = req.parameterMap.mapValues { it.value.first() },
                body = ""
            )
            val accessUrl = headers["forward-accessurl"]

            if (openApiConfig.usePolicyEnforcement && policyManager != null && accessUrl != null) {
                val contracts = policyManager.contractAgreementMatch(apiPath, recipient, sender, HttpMethod.valueOf(req.method))

                if (contracts.isEmpty()) {
                    LOG.info("No contractAgreement present, starting negotiation")
                    try {
                        val forwardPath = generateForwardPath(apiPath, accessUrl)
                        invokeOperationMessage.transferContract = policyNegotiation.contractOfferRequest(
                            forwardPath,
                            invokeOperationMessage,
                            payload,
                            receiver,
                            recipient,
                            sender
                        )
                    } catch (e: ContractNegotiationException) {
                        if (e.rejectionMessage != null) {
                            return@supplyAsync errorResponse(
                                mapRejectionReason(e.rejectionMessage.rejectionReason),
                                e.rejectionMessage.rejectionReason.toString(),
                                e.payload
                            )
                        } else {
                            return@supplyAsync ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("""{"error": "${e.message}"}""")
                        }
                    }
                } else {
                    LOG.info("Found matching contractAgreement ${contracts.first().id}")
                    invokeOperationMessage.transferContract = contracts.first().id
                }
            }
            if (LOG.isDebugEnabled) {
                LOG.debug("Header: {}", SerializationHelper.getInstance().toJsonLD(invokeOperationMessage))
                LOG.debug("Payload: {}", payload.encode())
            }

            val multiPartMessage = if (openApiConfig.encodeInHttp) {
                val path = if (payload.parameters.isNotEmpty()) {
                    apiPath + "?" + payload.parameters.map { "${it.key}=${it.value}" }.joinToString("&")
                } else {
                    apiPath
                }
                LOG.info("Using $API_PATH=$path & $API_METHOD=${payload.method}")
                val httpHeaders = mapOf(
                    TYPE to "HTTP-Encoded",
                    API_PATH to path,
                    API_METHOD to payload.method.toString()
                ) + payload.headers.mapKeys { "$HEADER_PREFIX${it.key}" }
                MultiPartMessage(
                    invokeOperationMessage,
                    req.inputStream,
                    httpHeaders = httpHeaders.toMutableMap()
                )
            } else {
                val completePayload = payload.copy(body = req.inputStream.readAllBytes().decodeToString())
                if (LOG.isDebugEnabled) {
                    LOG.debug("Payload: {}", payload.encode())
                }
                MultiPartMessage(
                    invokeOperationMessage,
                    completePayload.encode(),
                    contentType = "application/json"
                )
            }
            return@supplyAsync when {
                accessUrl?.startsWith("http") == true -> {
                    exchangeHttp(apiPath, accessUrl, multiPartMessage, "$callingBaseUrl/$apiVersion")
                }
                accessUrl != null -> {
                    exchangeIDSCP(multiPartMessage, "$callingBaseUrl/$apiVersion", accessUrl = accessUrl)
                }
                else -> {
                    exchangeIDSCP(multiPartMessage, "$callingBaseUrl/$apiVersion", receiver = receiver)
                }
            }
        }
    }

    private fun exchangeHttp(
        apiPath: String,
        accessUrl: String,
        requestMultiPartMessage: MultiPartMessage<*>,
        callingBaseUrl: String
    ): ResponseEntity<*> {
        val forwardPath = generateForwardPath(apiPath, accessUrl)
        val (statusCode, multiPartMessage) = httpHelper.toHTTP(
            forwardPath,
            requestMultiPartMessage
        )
        if (multiPartMessage.header is RejectionMessage) {
            return errorResponse(
                statusCode,
                (multiPartMessage.header as RejectionMessage).rejectionReason.toString(),
                multiPartMessage.payload?.asString()
            )
        }
        LOG.info(multiPartMessage.httpHeaders.toString())
        return if (multiPartMessage.httpHeaders.mapKeys { it.key.lowercase() }[TYPE.lowercase()] == "HTTP-Encoded") {
            decodeHttpPartToEntity(multiPartMessage, callingBaseUrl)
        } else {
            responseToEntity(multiPartMessage.payload?.asString() ?: "", callingBaseUrl)
        }
    }

    private fun exchangeIDSCP(
        requestMultiPartMessage: MultiPartMessage<*>,
        callingBaseUrl: String,
        accessUrl: String? = null,
        receiver: String? = null
    ): ResponseEntity<*> {
        val future = CompletableFuture<Pair<Message, InputStream?>>()
        responseManager.registerResponseHandler(requestMultiPartMessage.header.id.toString(), future)
        httpHelper.toIDSCP(
            accessUrl
                ?: receiver?.let { brokerClient.getConnector(it)?.hasDefaultEndpoint?.accessURL?.toString() }
                ?: receiver
                ?: throw IllegalArgumentException("Either accessUrl or receiver must be specified"),
            requestMultiPartMessage
        )

        return future.thenApply { (first, second): Pair<Message, InputStream?> ->
            if (first is RejectionMessage) {
                return@thenApply errorResponse(
                    mapRejectionReason(first.rejectionReason),
                    first.rejectionReason.toString(),
                    second?.readAllBytes()?.decodeToString()
                )
            }
            responseToEntity(second?.readAllBytes()?.decodeToString() ?: "", callingBaseUrl)
        }.get()
    }


    private fun decodeHttpPartToEntity(multiPartMessage: MultiPartMessage<*>, callingBaseUrl: String): ResponseEntity<*> {
        return multiPartMessage.payload?.let { part ->
            val lowercaseHeaders = multiPartMessage.httpHeaders.mapKeys { it.key.lowercase() }
            val headers = lowercaseHeaders
                .filterKeys { it.startsWith(HEADER_PREFIX.lowercase()) }
                .mapKeys { it.key.substring(HEADER_PREFIX.length) }
                .toMutableMap()
            val status = lowercaseHeaders[STATUS_CODE.lowercase()]?.toInt() ?: throw Exception("Received incorrect IDS OpenAPI response from remote connector")
            val backendMasked = lowercaseHeaders[BACKEND_MASKED.lowercase()] != null
            val httpHeaders = HttpHeaders()
            headers.forEach { (key, value) -> httpHeaders[key] = value }
            val payload: Any = if (backendMasked) {
                replaceMasks(part.asString(), callingBaseUrl)
            } else {
                part.inputStream()
            }

            ResponseEntity
                .status(status)
                .headers(httpHeaders)
                .body(payload)
        } ?: ResponseEntity.unprocessableEntity().body("")
    }

    private fun replaceMasks(payload: String, callingBaseUrl: String): String {
        return payload.replace(BACKEND_URL_MASK, callingBaseUrl)
    }

    private fun responseToEntity(payload: String, callingBaseUrl: String): ResponseEntity<String> {
        val responsePayload = SuccessResponseObject.decode(payload)
        val body = if (responsePayload.backendMasked) {
            replaceMasks(responsePayload.body, callingBaseUrl)
        } else {
            responsePayload.body
        }
        val responseHeaders = HttpHeaders()
        responsePayload.headers.forEach { (key, value) ->
            responseHeaders[key] = value
        }
        return ResponseEntity
            .status(responsePayload.status)
            .headers(responseHeaders)
            .body(body)
    }

    private fun errorResponse(statusCode: Int, reason: String, message: String?): ResponseEntity<String> {
        return ResponseEntity.status(statusCode).body(ErrorResponseObject(reason, message).encode())
    }

    private fun generateForwardPath(apiPath: String, accessUrl: String): String {
        return if (brokerPattern.matcher(apiPath).matches()) {
            constructAccessUrl(accessUrl, "broker")
        } else {
            accessUrl
        }
    }

    private fun applyCustomMappers(headers: MutableMap<String, String>): Boolean {
        return customMappers.map { (key, value) ->
            val headerValue = headers[key] ?: return@map false
            brokerClient.queryConnectors(value.replace("{{QUERY}}", headerValue))
            brokerClient.queryConnectors(value.replace("{{QUERY}}", headerValue)).firstOrNull()?.let { connector ->
                headers["forward-to"] = connector.id.toString()
                connector.resourceCatalog.mapNotNull { catalog ->
                    catalog.offeredResource.firstOrNull { resource ->
                        resource.sovereign.toString() == headerValue
                    }
                }.firstOrNull()?.let { resource ->
                    val accessUrl = resource.resourceEndpoint.first().accessURL ?: connector.hasDefaultEndpoint.accessURL
                    headers["forward-recipient"] = resource.sovereign.toString()
                    headers["forward-accessurl"] = constructAccessUrl(accessUrl, resource.resourceEndpoint.first().path)
                    return@map true
                }
            }
            false
        }.any { it }
    }

    companion object {
        private val LOG = LoggerFactory.getLogger(OpenAPIServer::class.java)
        private const val prefix = "/openapi"
        val restTemplate: RestTemplate = RestTemplateBuilder()
            .errorHandler(object : ResponseErrorHandler {
                override fun hasError(response: ClientHttpResponse): Boolean {
                    return false
                }
                override fun handleError(response: ClientHttpResponse) {
                }
            })
            .build()


        fun mapRejectionReason(rejectionReason: RejectionReason): Int {
            return when(rejectionReason) {
                RejectionReason.BAD_PARAMETERS, RejectionReason.MALFORMED_MESSAGE, RejectionReason.VERSION_NOT_SUPPORTED -> 400
                RejectionReason.NOT_AUTHENTICATED -> 401
                RejectionReason.NOT_AUTHORIZED -> 403
                RejectionReason.NOT_FOUND -> 404
                RejectionReason.MESSAGE_TYPE_NOT_SUPPORTED, RejectionReason.METHOD_NOT_SUPPORTED -> 405
                RejectionReason.TOO_MANY_RESULTS -> 429
                RejectionReason.INTERNAL_RECIPIENT_ERROR -> 500
                RejectionReason.TEMPORARILY_NOT_AVAILABLE -> 503
            }
        }
    }
}
