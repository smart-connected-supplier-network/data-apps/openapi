package nl.tno.ids.openapi.pef

import de.fraunhofer.iais.eis.*
import de.fraunhofer.iais.eis.util.TypedLiteral
import de.fraunhofer.iais.eis.util.Util
import nl.tno.ids.base.HttpHelper
import nl.tno.ids.base.configuration.IdsConfig
import nl.tno.ids.base.infomodel.defaults
import nl.tno.ids.base.infomodel.isResponseTo
import nl.tno.ids.base.infomodel.toJsonLD
import nl.tno.ids.base.infomodel.toMultiPartMessage
import nl.tno.ids.base.multipart.MultiPartMessage
import nl.tno.ids.common.serialization.DateUtil
import nl.tno.ids.common.serialization.SerializationHelper
import nl.tno.ids.openapi.ErrorResponseObject
import nl.tno.ids.openapi.OpenApiConfig
import nl.tno.ids.openapi.PolicyManager
import nl.tno.ids.openapi.RequestObject
import nl.tno.ids.openapi.client.ContractNegotiationException
import org.apache.http.entity.ContentType
import org.slf4j.LoggerFactory
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.lang.Nullable
import org.springframework.stereotype.Component
import java.net.URI
import java.util.*
import java.util.concurrent.ConcurrentHashMap

@Component
class PolicyNegotiation(
  private val httpHelper: HttpHelper,
  private val idsConfig: IdsConfig,
  private val openApiConfig: OpenApiConfig,
  @Nullable private val policyManager: PolicyManager?
) {
  companion object {
    private val LOG = LoggerFactory.getLogger(PolicyNegotiation::class.java)
  }

  val offersInflight: MutableMap<URI, ContractOffer> = ConcurrentHashMap()

  fun contractOfferRequest(
    forwardPath: String,
    invokeOperationMessage: InvokeOperationMessage,
    payload: RequestObject,
    receiver: String,
    recipient: String,
    sender: String
  ): URI {
    if (!openApiConfig.usePolicyEnforcement) {
      throw ContractNegotiationException("Policy enforcement is not enabled")
    }
    // Contract negotiation
    val (_, second) = httpHelper.toHTTP(
      forwardPath,
      invokeOperationMessage,
      payload.encode(),
      ContentType.APPLICATION_JSON
    )
    return when (val header = second.header) {
      is ContractOfferMessage -> {
        LOG.info("Received Contract Offer message")
        contractAgreementExchange(second, receiver, recipient, sender, forwardPath)
      }
      is ContractRejectionMessage -> {
        LOG.info("No suitable contract offer received: ${second.payload?.asString()}")
        throw ContractNegotiationException("No suitable contract offer received")
      }
      is RejectionMessage -> {
        LOG.info("Received rejection message: ${header.rejectionReason} ${second.payload?.asString()}")
        throw ContractNegotiationException("No suitable contract offer received", header, second.payload?.asString())
      }
      else -> {
        LOG.info("Unexpected message received: ${second.header.javaClass.name} -> ${second.payload?.asString()}")
        throw ContractNegotiationException("Unexpected message received: ${second.header.javaClass.name} -> ${second.payload?.asString()}")
      }
    }
  }

  private fun contractAgreementExchange(
    second: MultiPartMessage<*>,
    receiver: String,
    recipient: String,
    sender: String,
    forwardPath: String,
  ): URI {
    LOG.info("Creating contractAgreement")
    val contractOffer =
      SerializationHelper.getInstance().fromJsonLD(second.payload?.asString(), ContractOffer::class.java)
    val contractAgreement = createContractAgreement(contractOffer, recipient, sender)
    val contractAgreementMessage = ContractAgreementMessageBuilder()
      ._correlationMessage_(second.header.id)
      ._issuerConnector_(URI.create(idsConfig.connectorId))
      ._senderAgent_(URI.create(sender))
      ._recipientConnector_(Util.asList(URI.create(receiver)))
      ._recipientAgent_(Util.asList(URI.create(recipient)))
      .build()
      .defaults()
    val (_, second2) = httpHelper.toHTTP(
      forwardPath,
      contractAgreementMessage,
      contractAgreement
    )
    return when (second2.header) {
      is ContractAgreementMessage -> {
        LOG.info("Contract agreement accepted, storing agreement ${contractAgreement.id}")
        policyManager?.addContractAgreement(contractAgreement)
        contractAgreement.id
      }
      else -> {
        LOG.info("Contract agreement not accepted by provider: ${second2.payload?.asString()}")
        throw ContractNegotiationException("Contract agreement not accepted by provider")
      }
    }
  }

  private fun createContractAgreement(
    contractOffer: ContractOffer,
    recipient: String,
    sender: String
  ): ContractAgreement {
    return ContractAgreementBuilder()
      ._contractStart_(contractOffer.contractStart ?: xmlGregorianCalendar())
      ._contractEnd_(contractOffer.contractEnd ?: xmlGregorianCalendar(Calendar.MONTH, 3))
      ._contractDate_(contractOffer.contractDate ?: xmlGregorianCalendar())
      ._contractAnnex_(contractOffer.contractAnnex)
      ._contractDocument_(contractOffer.contractDocument)
      ._provider_(URI(recipient))
      ._consumer_(URI(sender))
      ._permission_(
        contractOffer.permission.map {
          it.assignee = arrayListOf(URI(sender))
          it.assigner = arrayListOf(URI(recipient))
          (it.postDuty + it.preDuty).forEach { duty ->
            duty.assignee = arrayListOf(URI(sender))
            duty.assigner = arrayListOf(URI(recipient))
          }
          it
        })
      ._prohibition_(
        contractOffer.prohibition.map {
          it.assignee = arrayListOf(URI(sender))
          it.assigner = arrayListOf(URI(recipient))
          it
        })
      ._obligation_(
        contractOffer.obligation.map {
          it.assignee = arrayListOf(URI(sender))
          it.assigner = arrayListOf(URI(recipient))
          it
        })
      .build()
  }

  /** XML Gregorian Calendar generator that adds a certain interval to the current time */
  private fun xmlGregorianCalendar(field: Int? = null, amount: Int? = null) =
    Calendar.getInstance()
      .apply {
        time = Date()
        if (field != null && amount != null) {
          add(field, amount)
        }
      }
      .time
      .run { DateUtil.asXMLGregorianCalendar(this) }

  fun contractOfferResponse(
    header: Message,
    apiPath: String,
    httpMethod: HttpMethod
  ): ResponseEntity<*> {
    if (header.transferContract != null) {
      val rejectionMessage = RejectionMessageBuilder()
        ._rejectionReason_(RejectionReason.NOT_AUTHORIZED)
        .build()
        .isResponseTo(header)
        .toMultiPartMessage(
          payload = "Request denied by Policy Enforcement, remove contract ${header.transferContract} to try renegotiation of a contract"
        )
      return ResponseEntity.status(HttpStatus.FORBIDDEN).body(rejectionMessage)
    }
    val offers = policyManager?.contractOfferMatches(
      apiPath = apiPath,
      assigner = header.recipientAgent.first().toString(),
      assignee = header.senderAgent.toString(),
      httpMethod = httpMethod
    ) ?: emptyList()
    when {
      offers.isEmpty() -> {
        LOG.info("No suitable contract offer available")
        LOG.debug("Context: ${header.issuerConnector}, $apiPath, ${header.recipientAgent.first()}, ${header.senderAgent}, $httpMethod")
        val contractRejectionMessage = ContractRejectionMessageBuilder()
          ._contractRejectionReason_(TypedLiteral("No suitable contract offer available", "en"))
          ._rejectionReason_(RejectionReason.NOT_FOUND)
          .build()
          .isResponseTo(header)
          .toMultiPartMessage(
            payload =
            ErrorResponseObject(
              status = 404,
              reason = "ContractOfferNotFound",
              message = "No suitable contract offer available for the given request"
            ).encode()
          )
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(contractRejectionMessage)
      }
      else -> {
        if (offers.size > 1) {
          LOG.warn("Multiple matching contractOffers found, selecting first: ${offers.map { it.id }}")
        } else {
          LOG.info("Received single matching contractOffer ${offers.first().id}")
        }
        val contractOfferMessage = ContractOfferMessageBuilder()
          .build()
          .isResponseTo(header)
        offersInflight[contractOfferMessage.id] = offers.first()
        val multiPartMessage = contractOfferMessage
          .toMultiPartMessage(
            payload = offers.first().toJsonLD(),
            contentType = "application/ld+json"
          )
        return ResponseEntity.ok(multiPartMessage)
      }
    }
  }



}