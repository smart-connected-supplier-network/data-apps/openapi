package nl.tno.ids.openapi.server

import de.fraunhofer.iais.eis.InvokeOperationMessage
import de.fraunhofer.iais.eis.Message
import de.fraunhofer.iais.eis.RejectionReason
import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.Operation
import io.swagger.v3.oas.models.PathItem
import io.swagger.v3.oas.models.PathItem.HttpMethod.*
import io.swagger.v3.parser.OpenAPIV3Parser
import nl.tno.ids.base.MessageHandler
import nl.tno.ids.base.ResponseBuilder
import nl.tno.ids.base.multipart.MultiPartMessage
import nl.tno.ids.openapi.*
import nl.tno.ids.openapi.IDSHttpHeaders.API_METHOD
import nl.tno.ids.openapi.IDSHttpHeaders.API_PATH
import nl.tno.ids.openapi.IDSHttpHeaders.BACKEND_MASKED
import nl.tno.ids.openapi.IDSHttpHeaders.HEADER_PREFIX
import nl.tno.ids.openapi.IDSHttpHeaders.STATUS_CODE
import nl.tno.ids.openapi.IDSHttpHeaders.TYPE
import nl.tno.ids.openapi.client.OpenAPIServer.Companion.mapRejectionReason
import nl.tno.ids.openapi.pef.PolicyNegotiation
import org.apache.http.client.methods.*
import org.apache.http.entity.InputStreamEntity
import org.apache.http.impl.client.CloseableHttpClient
import org.slf4j.LoggerFactory
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component
import java.io.IOException
import java.io.InputStream
import java.util.*

@Component
class OpenAPIMessageHandler(
    private val responseBuilder: ResponseBuilder,
    private val openApiConfig: OpenApiConfig,
    private val httpClient: CloseableHttpClient,
    private val policyNegotiation: PolicyNegotiation
) : MessageHandler<InvokeOperationMessage> {
    var agentMapping: Map<String, AgentMapping> = emptyMap()

    init {
        LOG.info("Starting OpenAPIMessageHandler")
    }

    fun safeConfigure(agents: Set<AgentConfig>) {
        for (i in 0..9) {
            try {
                configure(agents)
                return
            } catch (e: Exception) {
                LOG.warn("Cannot load OpenAPI definition: ${e.message}")
            }
            Thread.sleep(10000)
        }
    }

    private fun configure(agents: Set<AgentConfig>) {
        val loadedOpenAPIs = if (openApiConfig.validate) {
            val commonAPIs = openApiConfig.openApiBaseUrl?.let {
                openApiConfig.versions.map { version ->
                    LoadedOpenAPI(
                        openApiConfig.openApiBaseUrl,
                        version,
                        OpenAPIV3Parser().read(openApiConfig.openApiBaseUrl + version)
                    )
                }
            } ?: emptyList()
            val agentAPIs = agents.flatMap { agentConfig ->
                agentConfig.versions.map { version ->
                    val baseUrl = agentConfig.openApiBaseUrl ?: openApiConfig.openApiBaseUrl
                        ?: throw Exception("No OpenAPI Base URL found")
                    LoadedOpenAPI(
                        baseUrl,
                        version,
                        OpenAPIV3Parser().read(baseUrl + version).also {
                            if (version != it.info.version) {
                                LOG.warn("API spec version difference, filename is $version API spec is ${it.info.version}")
                            }
                        }
                    )
                }
            }
            commonAPIs + agentAPIs
        } else {
            emptyList()
        }
        agentMapping = agents.associate { agent ->
            val headers = (agent.backendAuthentication ?: openApiConfig.backendAuthentication)?.let { backendAuth ->
                when (backendAuth.type) {
                    AuthenticationType.BASIC ->
                        mapOf("Authorization" to "Basic ${Base64.getEncoder().encodeToString("${backendAuth.username}:${backendAuth.password}".toByteArray())}")
                    AuthenticationType.BEARER -> mapOf("Authorization" to "Bearer ${backendAuth.token}")
                    AuthenticationType.CUSTOM -> backendAuth.header
                }
            } ?: emptyMap()
            val versions = agent.versions.ifEmpty { openApiConfig.versions }.associateWith { version ->
                val backendUrl = when {
                    agent.backendUrlMapping.isNotEmpty() -> {
                        agent.backendUrlMapping[version] ?: throw Exception("No mapping found for agent ${agent.id} version $version")
                    }
                    agent.backendUrl != null -> {
                        "${agent.backendUrl}/$version"
                    }
                    openApiConfig.backendBaseUrlMapping.isNotEmpty() -> {
                        openApiConfig.backendBaseUrlMapping[version] ?: throw Exception("No base URL mapping found for version $version")
                    }
                    else -> "${openApiConfig.backendBaseUrl}/${agent.id}/$version"
                }
                val openApi = loadedOpenAPIs.find {
                    it.version == version &&
                        it.baseUrl == (agent.openApiBaseUrl ?: openApiConfig.openApiBaseUrl)
                }?.openAPI
                Endpoint(
                    openApi,
                    backendUrl
                )
            }
            agent.id to AgentMapping(
                agent,
                headers,
                versions
            )
        }
        LOG.info("Configured OpenAPIMessageHandler")
        LOG.info("Registered agents: ${agentMapping.keys.size}")
        LOG.info(
            "Configured agents:\n${
                agentMapping.toList().joinToString("\n") { agent ->
                    "${agent.first}:\n" + agent.second.versions.toList().joinToString("\n") {
                        "  - ${it.first} -> ${it.second.backendUrl} (${it.second.openAPI?.info?.title} ${it.second.openAPI?.info?.version})"
                    }
                }
            }"
        )
    }

    override fun handle(multiPartMessage: MultiPartMessage<InvokeOperationMessage>): ResponseEntity<*> {
        val header = multiPartMessage.header
        val partPayload = multiPartMessage.payload?.inputStream()
        val lowerCaseHeaders = multiPartMessage.httpHeaders.mapKeys { it.key.lowercase() }
        val (request, payload) = if (lowerCaseHeaders[TYPE.lowercase()] == "HTTP-Encoded") {
            LOG.info("Received HTTP Encoded OpenAPI call")
            val headers = lowerCaseHeaders.filterKeys { it.startsWith(HEADER_PREFIX.lowercase()) }
                .map { it.key.substring(HEADER_PREFIX.length) to it.value }
                .toMap()

            LOG.info("Headers: $headers")
            val path = lowerCaseHeaders[API_PATH.lowercase()] ?: return createRejectionEntity(header, RejectionReason.MALFORMED_MESSAGE, "No API Path header found")
            val pathSplit = path.split("?", limit = 2)
            val apiPath = pathSplit.first()
            val parameters = if (pathSplit.size > 1) {
                pathSplit.last().split("&").associate {
                    val parameter = it.split("=")
                    parameter.first() to parameter.last()
                }
            } else {
                emptyMap()
            }
            val apiMethod = lowerCaseHeaders[API_METHOD.lowercase()]?.let { PathItem.HttpMethod.valueOf(it) } ?: GET
            val requestObject = RequestObject(
                headers = headers,
                method = apiMethod,
                path = apiPath,
                parameters = parameters,
                body = null
            )
            LOG.info("Constructed RequestObject: $requestObject")
            requestObject to partPayload
        } else {
            val body = partPayload?.readAllBytes()?.decodeToString() ?: ""
            try {
                RequestObject.decode(body).let {
                    it to it.body?.byteInputStream()
                }
            } catch (e: Exception) {
                LOG.warn("Could not parse JSON-encoded request.")
                LOG.info(multiPartMessage.httpHeaders.toString())
                LOG.info(body)
                return createRejectionEntity(header, RejectionReason.MALFORMED_MESSAGE, "Could not parse JSON-encoded request.")
            }
        }
        val senderAgent = header.senderAgent.toString()
        val recipientAgent = header.recipientAgent[0].toString()

        LOG.info("Received invoke operation message for ${request.method} call on path ${request.apiPath}")

        val recipientBase = agentMapping[recipientAgent] ?: return createRejectionEntity(
            header,
            RejectionReason.BAD_PARAMETERS,
            "Can't find recipient agent $recipientAgent"
        )

        val backendEndpoint = recipientBase.versions[request.apiVersion] ?: return createRejectionEntity(
            header,
            RejectionReason.VERSION_NOT_SUPPORTED,
            "Recipient $recipientAgent does not support api version ${request.apiVersion}"
        )

        val (parameters: MutableMap<String, String>, headers: MutableMap<String, String>) = if (openApiConfig.validate) {
            val api = backendEndpoint.openAPI ?: return createRejectionEntity(
                header,
                RejectionReason.VERSION_NOT_SUPPORTED,
                "Didn't parse OpenAPI specification for version ${request.apiVersion} for agent $recipientAgent"
            )

            // Find OpenAPI operation belonging to the request
            val operation = getPathItem(api, request.apiPath)?.readOperationsMap()?.get(request.method)
                ?: return createRejectionEntity(
                    header,
                    RejectionReason.METHOD_NOT_SUPPORTED,
                    "Did not find combination of path \"${request.apiPath}\" with method \"${request.method}\" in API specification version \"${request.apiVersion}\""
                )

            // Retrieve parameters
            constructParameters(
                request.parameters,
                request.headers,
                operation,
                senderAgent,
                recipientAgent
            )
        } else {
            Pair(request.parameters.toMutableMap(), request.headers.toMutableMap().apply {
                this["Forward-Sender"] = senderAgent
                this["Forward-Sovereign"] = recipientAgent
            })
        }

        if (openApiConfig.usePolicyEnforcement && lowerCaseHeaders["PEF_DECISION".lowercase()] == "DENY") {
            LOG.info("Received PEF denied request, sending back ContractOffer if applicable")
            return policyNegotiation.contractOfferResponse(header, "/${request.apiVersion}/${request.apiPath}", HttpMethod.valueOf(request.method.name))
        }

        // Build up URL with potential query parameters
        val url = generateUrl(parameters, backendEndpoint.backendUrl, request.apiPath)
        val tsgHeaders = mapOf(
            "TSG_ISSUER_CONNECTOR" to header.issuerConnector.toString(),
            "TSG_RECIPIENT_CONNECTOR" to header.recipientConnector.first().toString(),
            "TSG_CONTRACT_ID" to (header.transferContract?.toString() ?: "NONE")
        )
        val proxyRequest = generateHttpRequest(request, payload, headers + recipientBase.headers + tsgHeaders, url)
        LOG.info("Calling ${request.method} $url (headers: $headers)")
        return try {
            httpClient.execute(proxyRequest).use { response ->
                val statusCode = response.statusLine.statusCode
                val (body, masked) = maskBackendUrl(response.entity.content, backendEndpoint.backendUrl)
                val responseHeaders = response.allHeaders.filterNot { connectionHeaders.contains(it.name) }.associate { it.name to it.value }
                val responseMultipartMessage = if (openApiConfig.encodeInHttp) {
                    val responseHttpHeaders = mutableMapOf(
                        TYPE to "HTTP-Encoded",
                        STATUS_CODE to statusCode.toString()
                    )
                    if (masked > 0) {
                        responseHttpHeaders[BACKEND_MASKED] = "true"
                    }
                    responseHttpHeaders += responseHeaders.mapKeys { "$HEADER_PREFIX${it.key}" }

                    MultiPartMessage(
                        header = responseBuilder.createResultMessage(header).build(),
                        payload = body,
                        httpHeaders = responseHttpHeaders
                    )
                } else {
                    MultiPartMessage(
                        responseBuilder.createResultMessage(header).build(),
                        payload = SuccessResponseObject(
                            status = statusCode,
                            body = body.readAllBytes().decodeToString(),
                            headers = responseHeaders,
                            backendMasked = masked > 0
                        ).encode(),
                        contentType = "application/json"
                    )
                }
                LOG.info("Response HTTP headers: ${responseMultipartMessage.httpHeaders}")
                val responseEntity = ResponseEntity.status(HttpStatus.OK)
                responseMultipartMessage.httpHeaders.forEach { (key, value) ->
                    responseEntity.header(key, value)
                }
                responseEntity.body(responseMultipartMessage)
            }
        } catch (e: IOException) {
            e.printStackTrace()
            createRejectionEntity(header, RejectionReason.INTERNAL_RECIPIENT_ERROR, e.message)
        }
    }

    private fun maskBackendUrl(inputStream: InputStream, backendUrl: String): Pair<InputStream, Int> {
        return if (openApiConfig.maskBackendUrl) {
            val body = inputStream.readAllBytes().decodeToString()
            val maskedBodySplit = body.split(backendUrl)
            val maskedBody = maskedBodySplit.joinToString(BACKEND_URL_MASK)
            maskedBody.byteInputStream() to maskedBodySplit.size-1
        } else {
            inputStream to 0
        }
    }

    private fun constructParameters(
        parameters: Map<String, String>,
        headers: Map<String, String>,
        operation: Operation,
        senderAgent: String,
        recipientAgent: String
    ): Pair<MutableMap<String, String>, MutableMap<String, String>> {
        val queryParams: MutableMap<String, String> = mutableMapOf()
        val headerParams: MutableMap<String, String> = mutableMapOf()
        if (openApiConfig.strictApi) {
            operation.parameters?.forEach { p ->
                if (p.getIn() == "query") {
                    parameters[p.name]?.let { queryParams[p.name] = it }
                } else if (p.getIn() == "header") {
                    headers[p.name]?.let { headerParams[p.name] = it }
                }
            }
        } else {
            headerParams += headers
            queryParams += parameters
        }

        // Default headers
        headers["content-type"]?.let { headerParams["content-type"] = it }
        headers["accept"]?.let { headerParams["accept"] = it }
        headers["authorization"]?.let { headerParams["authorization"] = it }
        headerParams["Forward-Sender"] = senderAgent
        headerParams["Forward-Sovereign"] = recipientAgent
        headerParams["Forward-ID"] = recipientAgent
        return Pair(queryParams, headerParams)
    }

    private fun generateHttpRequest(
        requestObject: RequestObject,
        inputStream: InputStream?,
        headerParams: Map<String, String>,
        url: String
    ): HttpRequestBase {
        val encodedUrl = url.replace(" ", "%20")
        val request = when (requestObject.method) {
            GET -> HttpGet(encodedUrl)
            DELETE -> HttpDelete(encodedUrl)
            HEAD -> HttpHead(encodedUrl)
            OPTIONS -> HttpOptions(encodedUrl)
            TRACE -> HttpTrace(encodedUrl)
            POST -> HttpPost(encodedUrl)
            PUT -> HttpPut(encodedUrl)
            PATCH -> HttpPatch(encodedUrl)
        }
        headerParams.forEach { request.setHeader(it.key, it.value) }
        if (request is HttpEntityEnclosingRequestBase) {
            request.entity = InputStreamEntity(inputStream ?: InputStream.nullInputStream())
        }
        return request
    }

    private fun generateUrl(queryParams: Map<String, String>, base: String, path: String): String {
        var url = constructAccessUrl(base, path)
        if (queryParams.isNotEmpty()) {
            url += "?" + queryParams.map { "${it.key}=${it.value}" }.joinToString("&")
        }
        return url
    }

    private fun getPathItem(api: OpenAPI, apiPath: String): PathItem? {
        var pathItem: PathItem? = null
        for ((key, value) in api.paths) {
            val match = pathMatch(apiPath, key)
            if (match == 2) {
                pathItem = value
                break
            } else if (match == 1) {
                pathItem = value
            }
        }
        return pathItem
    }

    private fun pathMatch(path: String, spec: String): Int {
        val pathSplit = path.dropWhile { it == '/' }.split("/".toRegex()).toTypedArray()
        val specSplit = spec.dropWhile { it == '/' }.split("/".toRegex()).toTypedArray()
        if (pathSplit.size != specSplit.size) return 0
        var soft = false
        for (i in pathSplit.indices) {
            if (pathSplit[i] != specSplit[i]) {
                val regex = specSplit[i].replace("([\\(\\)\\*\\.\\[\\]\\\$\\<\\>])".toRegex()) { "\\${it.groupValues[0]}" }.replace("\\{.*?\\}".toRegex(), ".*").toRegex()
                soft = if (regex.matches(pathSplit[i])) {
                    true
                } else {
                    return 0
                }
            }
        }
        return if (soft) 1 else 2
    }

    private fun createRejectionEntity(
        message: Message,
        reason: RejectionReason,
        explanation: String?
    ): ResponseEntity<*> {
        LOG.warn(explanation)
        return ResponseEntity
            .status(mapRejectionReason(reason))
            .body(
                MultiPartMessage(
                    header = responseBuilder.createRejectionMessage(message, reason).build(),
                    payload = explanation
                )
            )
    }

    companion object {
        private val LOG = LoggerFactory.getLogger(OpenAPIMessageHandler::class.java)
        private val connectionHeaders = listOf("Date", "Content-Length", "Connection", "Strict-Transport-Security", "X-Forwarded-For", "X-Forwarded-Host", "X-Forwarded-Port", "X-Forwarded-Proto", "X-Forwarded-Prefix", "Transfer-Encoding")

    }
}
