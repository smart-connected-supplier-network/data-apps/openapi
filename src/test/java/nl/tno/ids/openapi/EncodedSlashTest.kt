package nl.tno.ids.openapi

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock.*
import com.mongodb.client.MongoCollection
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.HttpClients
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.server.LocalServerPort
import org.springframework.test.context.TestPropertySource


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = ["spring.config.location=classpath:application-encoded-slash.yaml"])
@EnableAutoConfiguration(exclude = [MongoAutoConfiguration::class, MongoDataAutoConfiguration::class])
class EncodedSlashTest {

    @LocalServerPort
    private lateinit var port: String

    @Test
    fun testEncodedSlash() {
        HttpClients.createDefault().execute(HttpGet("http://localhost:$port/openapi/v1/shells/www.ijssel.com%2FIdlerRoller200-610060/aas/submodels").apply {
            setHeader("Forward-To", "urn:ids:connector")
            setHeader("Forward-Recipient", "urn:aas:1234")
            setHeader("Forward-Sender", "urn:aas:1234")
            setHeader("Forward-Accessurl", "http://localhost:$port/router")
        }).use {
            Assertions.assertEquals(500, it.statusLine.statusCode)
        }
    }

    companion object {
        private lateinit var wiremock: WireMockServer
        private lateinit var collection: MongoCollection<AgentConfig>

        @BeforeAll
        @JvmStatic
        fun setupWireMock() {
            wiremock = WireMockServer(47375).apply { start() }
            wiremock.stubFor(
                any(anyUrl()).willReturn(ok())
            )
        }

        @AfterAll
        @JvmStatic
        fun destroyWireMock() {
            wiremock.stop()
        }
    }
}