package nl.tno.ids.openapi

import de.fraunhofer.iais.eis.ContractOffer
import de.fraunhofer.iais.eis.ContractOfferBuilder
import de.fraunhofer.iais.eis.PermissionBuilder
import de.fraunhofer.iais.eis.ProhibitionBuilder
import nl.tno.ids.base.infomodel.toJsonLD
import nl.tno.ids.common.serialization.DateUtil
import nl.tno.ids.common.serialization.SerializationHelper
import org.junit.jupiter.api.Test
import org.springframework.http.HttpMethod
import java.net.URI

class PolicyEnforcementTest {

  @Test
  fun testSerialization() {
    val policyOffer = PolicyOffer(
      providerConnector = "urn:ids:connector",
      rules = listOf(
        Rule(
          endpoint = "order.*",
          methods = listOf(HttpMethod.GET, HttpMethod.POST),
          assignerAgent = "urn:ids:assigner",
          assigneeAgent = "urn:ids:assignee",
          permission = Permission.ALLOW
        ),
        Rule(
          endpoint = "orderstatus",
          methods = listOf(HttpMethod.GET, HttpMethod.POST),
          assignerAgent = "urn:ids:assigner",
          permission = Permission.DENY
        )
      )
    )

    val contractOfferBuilder = ContractOfferBuilder()
      ._contractStart_(DateUtil.now())
    policyOffer.consumerConnector?.let {
      contractOfferBuilder._consumer_(URI(it))
    }
    policyOffer.providerConnector?.let {
      contractOfferBuilder._provider_(URI(it))
    }
    policyOffer.rules.forEach { rule ->
      val target = "urn:regex:/${rule.version ?: ".*"}/${rule.endpoint ?: ".*"}"
      when (rule.permission) {
        Permission.ALLOW -> {
          contractOfferBuilder._permission_(
            PermissionBuilder()
            ._action_(rule.methods.map { PolicyManager.mapMethodToAction(it) })
            ._target_(URI(target))
            .apply {
              rule.assigneeAgent?.let {
                _assignee_(URI(it))
              }
              rule.assignerAgent?.let {
                _assigner_(URI(it))
              }
            }
            .build())
        }
        Permission.DENY -> {
          contractOfferBuilder._prohibition_(
            ProhibitionBuilder()
            ._action_(rule.methods.map { PolicyManager.mapMethodToAction(it) })
            ._target_(URI(target))
            .apply {
              rule.assigneeAgent?.let {
                _assignee_(URI(it))
              }
              rule.assignerAgent?.let {
                _assigner_(URI(it))
              }
            }
            .build())
        }
      }
    }

    val contractOffer = contractOfferBuilder.build()
    val contractOfferString = contractOffer.toJsonLD()

    SerializationHelper.getInstance().fromJsonLD(contractOfferString, ContractOffer::class.java)

    println(contractOfferString)
  }

}